FROM dtzar/helm-kubectl:3.2.4

RUN apk add \
git \
openssh \
jq \
py3-pip \
nodejs \
python3

RUN ln -s $(which python3) /usr/bin/python

# Yq
RUN wget -q -O /usr/bin/yq $(wget -q -O - https://api.github.com/repos/mikefarah/yq/releases/latest | \
    jq -r '.assets[] | select(.name == "yq_linux_amd64") | .browser_download_url') \
    && chmod +x /usr/bin/yq

RUN helm plugin install https://github.com/mbenabda/helm-local-chart-version

RUN pip3 install awscli